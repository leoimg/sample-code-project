## LibraryBooks App Challenge

Remember to do pod init to install dependencies and remember to launch .xcworkspace instead of .xcodeproj

---

### Networking Library
1. This App uses Alamofire [Alamofire](https://github.com/Alamofire/Alamofire/blob/master/README.md)

---

### Features
1. List all books in your library.
2. Add a new book.
3. View the book details.
4. Checkout a book.
5. Delete individual books from the library.
6. Delete all books from the library.

---

### Architecture Details
1. The project where applicable uses Model View Controller structure.
2. The project does not use Storyboard, instead uses Code for design.
3. All network calls are done through three main functions for code reusability.
4. Auto Layout is used throughout project. You can rotate the screen in any direction and the app still looks good.

---

### Screen 1 (Library)
1. For screen 1 I split the GET API in it's own file to keep everything clean and easy to read.
2. Splitting it also allows to better focus on the data model being built.

### Screen 2 (New Book)
1. The keyboard return key sends to the next TextField when tapped and if on the last one it offers the Done return key which calls the API to add the new book.
2. The return key makes it effortless to go to the next required textfield. UX is crucial.
