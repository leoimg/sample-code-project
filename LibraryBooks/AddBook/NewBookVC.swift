//
//  NewBookTableVC.swift
//  LibraryBooks
//
//  Created by LG on 8/8/18.
//  Copyright © 2018 Leonardo Gjoni. All rights reserved.
//

import UIKit

class NewBookVC: UIViewController {
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.flatWhite
        setupNavigationBar()
        textFieldDelegates()
    }
    
    let bookTitleTextView: UITextField = {
        let textView = UITextField()
        textView.placeholder = "Book Title"
        textView.backgroundColor = .white
        textView.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.layer.cornerRadius = 10
        textView.layer.borderWidth = 1
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.becomeFirstResponder()
        return textView
    }()
    
    let authorTextView: UITextField = {
        let textView = UITextField()
        textView.placeholder = "Author"
        textView.backgroundColor = .white
        textView.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.layer.cornerRadius = 10
        textView.layer.borderWidth = 1
        textView.layer.borderColor = UIColor.lightGray.cgColor
        return textView
    }()
    
    let publisherTextView: UITextField = {
        let textView = UITextField()
        textView.placeholder = "Publisher"
        textView.backgroundColor = .white
        textView.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.layer.cornerRadius = 10
        textView.layer.borderWidth = 1
        textView.layer.borderColor = UIColor.lightGray.cgColor
        return textView
    }()
    
    let categoriesTextView: UITextField = {
        let textView = UITextField()
        textView.placeholder = "Categories"
        textView.backgroundColor = .white
        textView.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0)
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.layer.cornerRadius = 10
        textView.layer.borderWidth = 1
        textView.layer.borderColor = UIColor.lightGray.cgColor
        return textView
    }()
    
    let submitButton: UIButton = {
        let button = UIButton()
        button.setTitle("Submit", for: .normal)
        button.backgroundColor = .blue
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 10
        button.addTarget(self, action: #selector(addNewBook), for: .touchUpInside)
        return button
    }()
    
    
    
    
    func setupViews() {
        self.view.addSubview(bookTitleTextView)
        bookTitleTextView.topAnchor.constraint(equalTo: view.topAnchor, constant: 30).isActive = true
        bookTitleTextView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive = true
        bookTitleTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        bookTitleTextView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30).isActive = true
        
        self.view.addSubview(authorTextView)
        authorTextView.topAnchor.constraint(equalTo: bookTitleTextView.bottomAnchor, constant: 10).isActive = true
        authorTextView.leadingAnchor.constraint(equalTo: bookTitleTextView.leadingAnchor).isActive = true
        authorTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        authorTextView.trailingAnchor.constraint(equalTo: bookTitleTextView.trailingAnchor).isActive = true
        
        self.view.addSubview(publisherTextView)
        publisherTextView.topAnchor.constraint(equalTo: authorTextView.bottomAnchor, constant: 10).isActive = true
        publisherTextView.leadingAnchor.constraint(equalTo: authorTextView.leadingAnchor).isActive = true
        publisherTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        publisherTextView.trailingAnchor.constraint(equalTo: authorTextView.trailingAnchor).isActive = true
        
        self.view.addSubview(categoriesTextView)
        categoriesTextView.topAnchor.constraint(equalTo: publisherTextView.bottomAnchor, constant: 10).isActive = true
        categoriesTextView.leadingAnchor.constraint(equalTo: publisherTextView.leadingAnchor).isActive = true
        categoriesTextView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        categoriesTextView.trailingAnchor.constraint(equalTo: publisherTextView.trailingAnchor).isActive = true
        
        self.view.addSubview(submitButton)
        submitButton.topAnchor.constraint(equalTo: categoriesTextView.bottomAnchor, constant: 10).isActive = true
        submitButton.leadingAnchor.constraint(equalTo: categoriesTextView.leadingAnchor).isActive = true
        submitButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        submitButton.trailingAnchor.constraint(equalTo: categoriesTextView.trailingAnchor).isActive = true
    }
    
    
    // MARK: - Functions
    func setupAddBookButton() {
        let dismissBtn = UIButton(type: .custom)
        dismissBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        let origImage = UIImage(named: "letter-x")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        dismissBtn.setImage(tintedImage, for: .normal)
        dismissBtn.addTarget(self, action: #selector(checkFieldsBeforeDismissVC), for: UIControlEvents.touchUpInside)
        dismissBtn.tintColor = .blue
        
        let menuBarItem = UIBarButtonItem(customView: dismissBtn)
        menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 20).isActive = true
        menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 20).isActive = true
        self.navigationItem.rightBarButtonItem = menuBarItem
    }
    
    @objc func dismissVC() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func checkFieldsBeforeDismissVC() {
        
        if authorTextView.text != "" || categoriesTextView.text != "" || bookTitleTextView.text != "" || publisherTextView.text != "" {
            alertController(title: "Exit?", message: "We detected some book information. Are you sure you want to exit without adding the book? ")
        } else {
            self.view.endEditing(true)
            dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func addNewBook() {
        
        if bookTitleTextView.text == "" {
            emptyFields(title: "Empty Title", message: "Please enter a title for the book.")
        } else if authorTextView.text == "" {
            emptyFields(title: "Empty Author", message: "Please add an author for the book.")
        } else {
            let params: Dictionary<String, Any> = [
                "author": authorTextView.text ?? "",
                "categories": categoriesTextView.text ?? "",
                "title": bookTitleTextView.text ?? "",
                "publisher": publisherTextView.text ?? "",
                ]
            
            let url = "\(NetworkingService.apiUrl)/books"
            
            NetworkingService.shared.callServerWithParams(withData: url, parameters: params, method: .post) { (json) in
                if let _ = json as? [String: Any] {
                    UserDefaults.standard.set(true, forKey: "refreshLibrary")
                    self.successAlert(title: "Success!", message: "Your new book has been added in the library.")
                }
            }
        }
    }
    
    func successAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (UIAlertAction) in
            self.dismissVC()
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    func emptyFields(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (UIAlertAction) in
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    func alertController(title: String, message: String) {
        let emptyFields = UIAlertController(title: title, message: message, preferredStyle: .alert)
        emptyFields.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (UIAlertAction) in
        }))
        emptyFields.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (UIAlertAction) in
            self.dismissVC()
        }))
        present(emptyFields, animated: true, completion: nil)
    }
    
    func setupNavigationBar() {
        self.title = "New Book"
        self.navigationController?.navigationBar.isTranslucent = false
        setupAddBookButton()
        setupViews()
    }
    
}

extension NewBookVC: UITextFieldDelegate {
    func textFieldDelegates() {
        bookTitleTextView.delegate = self
        authorTextView.delegate = self
        publisherTextView.delegate = self
        categoriesTextView.delegate = self
        
        bookTitleTextView.returnKeyType = .next
        authorTextView.returnKeyType = .next
        publisherTextView.returnKeyType = .next
        categoriesTextView.returnKeyType = .done
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isEqual(self.bookTitleTextView) {
            authorTextView.becomeFirstResponder()
        }
        
        if textField.isEqual(self.authorTextView) {
            publisherTextView.becomeFirstResponder()
        }
        
        if textField.isEqual(self.publisherTextView) {
            categoriesTextView.becomeFirstResponder()
        }
        
        if textField.isEqual(self.categoriesTextView) {
            addNewBook()
        }
        
        return true
        
    }
}


