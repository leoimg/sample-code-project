//
//  ColorExtensions.swift
//  LibraryBooks
//
//  Created by LG on 8/8/18.
//  Copyright © 2018 Leonardo Gjoni. All rights reserved.
//

import Foundation
import UIKit

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func random() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
    
    static let flatWhite = UIColor(red: 247/255, green: 248/255, blue: 249/255, alpha: 1.0)
}


