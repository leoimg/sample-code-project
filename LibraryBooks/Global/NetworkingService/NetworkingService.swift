//
//  NetworkingService.swift
//  LibraryBooks
//
//  Created by LG on 8/9/18.
//  Copyright © 2018 Leonardo Gjoni. All rights reserved.
//

import Foundation
import Alamofire

class NetworkingService {
    
    static let apiUrl = "https://ivy-ios-challenge.herokuapp.com"

    static let shared = NetworkingService()

    func callServer(withData url: String, method: HTTPMethod, completion: @escaping (Any) -> Void) {
        
        Alamofire.request(url, method: method).validate().responseJSON { response in
            switch response.result {
            case .success:
                completion(response.result.value as Any)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func callServerStatus200(withData url: String, method: HTTPMethod, completion: @escaping (Int) -> Void) {
        
        Alamofire.request(url, method: method)
            .validate(statusCode: 200..<300)
            .responseData { response in
                switch response.result {
                case .success:
                    completion((response.response?.statusCode)!)
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    func callServerWithParams(withData url: String, parameters: Parameters, method: HTTPMethod, completion: @escaping (Any)->Void) {
        
        Alamofire.request(url, method: method, parameters: parameters).validate().responseJSON { response in
            switch response.result {
            case .success:
                completion(response.result.value as Any)
            case .failure(let error):
                print(error)
            }
        }
    }
}
