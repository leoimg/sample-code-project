
import UIKit

class BookDetailsVC: UIViewController {
    
    var bookDetails: BooksLibraryModel?
    
    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.flatWhite
        //self.title = "New Book"
        self.navigationController?.navigationBar.isTranslucent = false
        setupBackButton()
        setupShareButton()
        setupViews()
        
        if bookDetails != nil {
            configure(withDataFrom: bookDetails!)
        } else {
            dismissVC()
        }
        
    }
    
    func configure(withDataFrom book: BooksLibraryModel) {
        bookTitleLabel.text = book.title
        authorLabel.text = "Author: \(String(describing: book.author!))"
        publisherLabel.text = "Publisher: \(String(describing: book.publisher!))"
        categoryLabel.text = "Category: \(String(describing: book.categories!))"
        
        if book.title != nil {
            let firstLetter = String(describing: book.title!.prefix(1))
            bookCoverLetter.text = firstLetter
        }
        
        if book.lastCheckedOut != nil && book.lastCheckedOutBy != nil {
            let lastCheckout = "At: \(book.lastCheckedOut!)"
            let lastCheckoutBy = "Checkout By: \(book.lastCheckedOutBy!)"
            checkoutLabel.text = "\(lastCheckoutBy), \(lastCheckout)"
        }
    }
    
    let bookCoverImage: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.random()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 4
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let bookCoverLetter: UILabel = {
        let faintWhitecolor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.7)
        
        let label = UILabel()
        label.font = UIFont(name: "Helvetica-bold", size: 34)
        label.textColor = faintWhitecolor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let bookTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "The Core iOS Developer's Cookbook"
        label.font = UIFont(name: "Helvetica", size: 18)
        label.numberOfLines = 0
        label.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let authorLabel: UILabel = {
        let label = UILabel()
        label.text = "Author:"
        label.textColor = .gray
        label.font = UIFont(name: "Helvetica", size: 14)
        label.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let publisherLabel: UILabel = {
        let label = UILabel()
        label.text = "Publisher:"
        label.textColor = .gray
        label.font = UIFont(name: "Helvetica", size: 14)
        label.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let categoryLabel: UILabel = {
        let label = UILabel()
        label.text = "Tags: Swift"
        label.textColor = .gray
        label.font = UIFont(name: "Helvetica", size: 14)
        label.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let checkoutLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.numberOfLines = 0
        label.font = UIFont(name: "Helvetica", size: 14)
        label.layer.sublayerTransform = CATransform3DMakeTranslation(12, 0, 0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let submitButton: UIButton = {
        let button = UIButton()
        button.setTitle("Checkout", for: .normal)
        button.backgroundColor = .blue
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(checkOutbook), for: .touchUpInside)
        button.layer.cornerRadius = 10
        return button
    }()
    
    
    // MARK: - Functions
    func setupBackButton() {
        let backbtn = UIButton(type: .custom)
        backbtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        let origImage = UIImage(named: "left-arrow")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        backbtn.setImage(tintedImage, for: .normal)
        backbtn.addTarget(self, action: #selector(dismissVC), for: UIControlEvents.touchUpInside)
        backbtn.tintColor = .blue
        
        let leftMenuBarItem = UIBarButtonItem(customView: backbtn)
        leftMenuBarItem.customView?.widthAnchor.constraint(equalToConstant: 25).isActive = true
        leftMenuBarItem.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        
        self.navigationItem.leftBarButtonItem = leftMenuBarItem
    }
    
    @objc func dismissVC() {
        navigationController?.popViewController(animated: true)
    }
    
    func setupShareButton() {
        let shareBtn = UIButton(type: .custom)
        shareBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        let origImage = UIImage(named: "share")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        shareBtn.setImage(tintedImage, for: .normal)
        shareBtn.addTarget(self, action: #selector(shareFeature), for: UIControlEvents.touchUpInside)
        shareBtn.tintColor = .blue
        
        let leftMenuBarItem = UIBarButtonItem(customView: shareBtn)
        leftMenuBarItem.customView?.widthAnchor.constraint(equalToConstant: 25).isActive = true
        leftMenuBarItem.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        self.navigationItem.rightBarButtonItem = leftMenuBarItem
    }
    
    @objc func shareFeature() {
        
        let string1 = "I'm reading this book called \(bookDetails!.title!)"
        let string2 = "written by: \(bookDetails!.author!) and you have to check it out!"
        
        let shareController = UIActivityViewController(activityItems: [string1, string2], applicationActivities: nil)
        present(shareController, animated: true, completion: nil)
    }
    
    @objc func checkOutbook() {
        let alertController = UIAlertController(title: "Full Name", message: "To checkout this book please enter your name.", preferredStyle: .alert)
        
        alertController.addTextField(configurationHandler: { (textField : UITextField!) in
            textField.placeholder = "John Doe"
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (UIAlertAction) in
                if textField.text != "" {
                    self.callNetwork(with: textField.text!)
                }
            }))
        })
        
        present(alertController, animated: true, completion: nil)
    }
    
    func callNetwork(with fullName: String) {
        guard let bookId = bookDetails?.id else { return }
        
        let url = "\(NetworkingService.apiUrl)/books/\(bookId)/"
        
        let params: Dictionary<String, Any> = [
            "lastCheckedOutBy": fullName
        ]
        
        NetworkingService.shared.callServerWithParams(withData: url, parameters: params, method: .put) { (json) in
            if let _ = json as? [String: Any] {
                UserDefaults.standard.set(true, forKey: "refreshLibrary")
                self.successBookAddedAlert(title: "Success!", message: "You've sucessfully checked out the book. Hope you enjoy it!")
            }
        }
    }
    
    func successBookAddedAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (UIAlertAction) in
            self.dismissVC()
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - View Design
    func setupViews() {
        self.view.addSubview(bookCoverImage)
        bookCoverImage.topAnchor.constraint(equalTo: view.topAnchor, constant: 30).isActive = true
        bookCoverImage.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive = true
        bookCoverImage.widthAnchor.constraint(equalToConstant: 110).isActive = true
        bookCoverImage.heightAnchor.constraint(equalToConstant: 155).isActive = true
        
        bookCoverImage.addSubview(bookCoverLetter)
        bookCoverLetter.centerXAnchor.constraint(equalTo: bookCoverImage.centerXAnchor).isActive = true
        bookCoverLetter.centerYAnchor.constraint(equalTo: bookCoverImage.centerYAnchor).isActive = true
        
        self.view.addSubview(bookTitleLabel)
        bookTitleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 30).isActive = true
        bookTitleLabel.leadingAnchor.constraint(equalTo: bookCoverImage.trailingAnchor, constant: 15).isActive = true
        //bookTitleLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        bookTitleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30).isActive = true
        
        self.view.addSubview(authorLabel)
        authorLabel.topAnchor.constraint(equalTo: bookTitleLabel.bottomAnchor, constant: 10).isActive = true
        authorLabel.leadingAnchor.constraint(equalTo: bookTitleLabel.leadingAnchor).isActive = true
        authorLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        authorLabel.trailingAnchor.constraint(equalTo: bookTitleLabel.trailingAnchor).isActive = true
        
        self.view.addSubview(publisherLabel)
        publisherLabel.topAnchor.constraint(equalTo: authorLabel.bottomAnchor, constant: 5).isActive = true
        publisherLabel.leadingAnchor.constraint(equalTo: authorLabel.leadingAnchor).isActive = true
        publisherLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        publisherLabel.trailingAnchor.constraint(equalTo: authorLabel.trailingAnchor).isActive = true
        
        self.view.addSubview(categoryLabel)
        categoryLabel.topAnchor.constraint(equalTo: publisherLabel.bottomAnchor, constant: 5).isActive = true
        categoryLabel.leadingAnchor.constraint(equalTo: publisherLabel.leadingAnchor).isActive = true
        categoryLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        categoryLabel.trailingAnchor.constraint(equalTo: publisherLabel.trailingAnchor).isActive = true
        
        
        
        self.view.addSubview(checkoutLabel)
        checkoutLabel.topAnchor.constraint(equalTo: bookCoverImage.bottomAnchor, constant: 15).isActive = true
        checkoutLabel.leadingAnchor.constraint(equalTo: bookCoverImage.leadingAnchor).isActive = true
        checkoutLabel.trailingAnchor.constraint(equalTo: publisherLabel.trailingAnchor).isActive = true
        
        self.view.addSubview(submitButton)
        submitButton.topAnchor.constraint(equalTo: checkoutLabel.bottomAnchor, constant: 15).isActive = true
        submitButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive = true
        submitButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        submitButton.trailingAnchor.constraint(equalTo: checkoutLabel.trailingAnchor).isActive = true
    }
    
    
}


