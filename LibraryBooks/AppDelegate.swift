//
//  AppDelegate.swift
//  LibraryBooks
//
//  Created by LG on 8/8/18.
//  Copyright © 2018 Leonardo Gjoni. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.makeKeyAndVisible()
        
        let bookLibraryVC = BookLibraryTableVC()
        let libraryController = UINavigationController(rootViewController: bookLibraryVC)
        self.window?.rootViewController = libraryController
        
        
        
        return true
    }

}

