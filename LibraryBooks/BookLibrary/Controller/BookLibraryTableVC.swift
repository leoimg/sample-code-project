//
//  ViewController.swift
//  LibraryBooks
//
//  Created by LG on 8/8/18.
//  Copyright © 2018 Leonardo Gjoni. All rights reserved.
//

import UIKit
import Alamofire

class BookLibraryTableVC: UITableViewController {
    
    var booksInLibrary = [BooksLibraryModel]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserDefaults.standard.value(forKey: "refreshLibrary") as? Bool == true {
            callNetwork()
            UserDefaults.standard.set(false, forKey: "refreshLibrary")
        }
    }

    // MARK: - View Did Load
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        self.title = "Library Books"
        tableView.separatorInset = UIEdgeInsets.zero
        setupAddBookbarButton()
        setupDeleteAllBarButton()
        callNetwork()
    }
    

    // MARK: - Reuse Identifiers
    let libraryListCell = "libraryListTVCell"

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return booksInLibrary.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let libraryCell = tableView.dequeueReusableCell(withIdentifier: libraryListCell, for: indexPath) as? LibraryListTVCell else { return UITableViewCell() }
        libraryCell.configure(withBookData: booksInLibrary[indexPath.row])
        return libraryCell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let navigator = navigationController {
            let bookDetailsVC = BookDetailsVC()
            bookDetailsVC.bookDetails = booksInLibrary[indexPath.row]
            navigator.pushViewController(bookDetailsVC, animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            guard let bookId = booksInLibrary[indexPath.row].id else { return }
            let bookIntToString = String(bookId)
            deleteBookIdNetwork(bookId: bookIntToString)
        }
    }
    
    // MARK: - Functions
    func registerCells() {
        tableView.register(LibraryListTVCell.self, forCellReuseIdentifier: libraryListCell)
    }
    
    func setupAddBookbarButton() {
        let addBtn = UIButton(type: .custom)
        addBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        let origImage = UIImage(named: "add-circular")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        addBtn.setImage(tintedImage, for: .normal)
        addBtn.addTarget(self, action: #selector(presentAddBook), for: UIControlEvents.touchUpInside)
        addBtn.tintColor = .blue
        
        let menuBarItem = UIBarButtonItem(customView: addBtn)
        menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 25).isActive = true
        menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.navigationItem.rightBarButtonItem = menuBarItem
    }
    
    func setupDeleteAllBarButton() {
        let deleteBtn = UIBarButtonItem(title: "Clear All", style: .plain, target: self, action: #selector(deleteAllActionSheet))
        self.navigationItem.leftBarButtonItem = deleteBtn
    }
    
    @objc func deleteAllActionSheet() {
        let alert = UIAlertController(title: "Clear Library", message: "Are you sure you want to clear the library?", preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive , handler:{ (UIAlertAction)in
            self.deleteAllBooksNetwork()
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: {
            //print("completion block")
        })
    }
    
    @objc func presentAddBook() {
        let newBookTableVC = NewBookVC()
        let newBookController = UINavigationController(rootViewController: newBookTableVC)
        present(newBookController, animated: true, completion: nil)
    }
    
    func callNetwork() {
        BookLibraryNetwork.shared.callNetwork { (response) in
            self.booksInLibrary = response.bookLibraryArrayModel
            self.tableView.reloadData()
        }
    }
    
    func deleteBookIdNetwork(bookId: String) {
        let url = "\(NetworkingService.apiUrl)/books/\(bookId)/"
        
        NetworkingService.shared.callServer(withData: url, method: .delete) { (json) in
            self.callNetwork()
        }
    }
    
    func deleteAllBooksNetwork() {
        let url = "\(NetworkingService.apiUrl)/clean"
        
        NetworkingService.shared.callServerStatus200(withData: url, method: .delete) { (response) in
            print("checking response:", response)
            if response == 200 {
                self.callNetwork()
            }
        }
    }

}

