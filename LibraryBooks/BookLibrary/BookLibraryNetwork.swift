//
//  BookLibraryNetork.swift
//  LibraryBooks
//
//  Created by LG on 8/9/18.
//  Copyright © 2018 Leonardo Gjoni. All rights reserved.
//

import Foundation

struct BookLibraryNetwork {
    static let shared = BookLibraryNetwork()
    
    func callNetwork(completion: @escaping(GetBookLibraryResponse) -> Void) {
        
        let url = "\(NetworkingService.apiUrl)/books"
        
        NetworkingService.shared.callServer(withData: url, method: .get) { (json) in
            do {
                let getBookLibraryResponse = try GetBookLibraryResponse(json: json)
                completion(getBookLibraryResponse)
            } catch { }
        }
    }
}

struct GetBookLibraryResponse {
    var bookLibraryArrayModel: [BooksLibraryModel]
    
    init(json: Any) throws {
    
        var booksArrayStored = [BooksLibraryModel]()
        if let booksArray = json as? [[String: Any]] {
            if booksArray.count > 0 {
                for book in booksArray {
                    guard let item = BooksLibraryModel(json: book) else { continue }
                    booksArrayStored.append(item)
                }
            }
        }
        self.bookLibraryArrayModel = booksArrayStored
    }
}
