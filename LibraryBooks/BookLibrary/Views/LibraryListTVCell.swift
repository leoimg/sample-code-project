//
//  LibraryListTVCell.swift
//  LibraryBooks
//
//  Created by LG on 8/8/18.
//  Copyright © 2018 Leonardo Gjoni. All rights reserved.
//

import UIKit

class LibraryListTVCell: UITableViewCell {
    
    let bookCoverImage: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.random()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 4
        imageView.clipsToBounds = true
        return imageView
    }()

    let bookCoverLetter: UILabel = {
        let faintWhitecolor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.7)
        
        let label = UILabel()
        label.font = UIFont(name: "Helvetica-bold", size: 24)
        label.textColor = faintWhitecolor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let bookTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 16)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let bookAuthor: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: 14)
        label.textColor = .lightGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        layoutViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        layoutViews()
        
    }
    
    func layoutViews() {

        contentView.addSubview(bookCoverImage)
        bookCoverImage.heightAnchor.constraint(equalToConstant: 70).isActive = true
        bookCoverImage.widthAnchor.constraint(equalToConstant: 55).isActive = true
        bookCoverImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 25).isActive = true
        bookCoverImage.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10).isActive = true
        bookCoverImage.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10).isActive = true
        
        bookCoverImage.addSubview(bookCoverLetter)
        bookCoverLetter.centerXAnchor.constraint(equalTo: bookCoverImage.centerXAnchor).isActive = true
        bookCoverLetter.centerYAnchor.constraint(equalTo: bookCoverImage.centerYAnchor).isActive = true
        
        contentView.addSubview(bookTitle)
        bookTitle.leadingAnchor.constraint(equalTo: bookCoverImage.trailingAnchor, constant: 10).isActive = true
        bookTitle.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15).isActive = true
        
        contentView.addSubview(bookAuthor)
        bookAuthor.leadingAnchor.constraint(equalTo: bookTitle.leadingAnchor).isActive = true
        bookAuthor.topAnchor.constraint(equalTo: bookTitle.bottomAnchor, constant: 8).isActive = true
    }
    
    func configure(withBookData book: BooksLibraryModel) {
        
        if book.title != nil {
            let firstLetter = String(describing: book.title!.prefix(1))
            bookCoverLetter.text = firstLetter
        }
        
        bookTitle.text = book.title
        bookAuthor.text = book.author
    }
    

}
